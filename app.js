var express = require('express')
let logger = require('morgan')
let cors = require('cors')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var path = require('path')
var api = require('./api')
var auth = require('./api/auth')

var app = express()

// set up our express application
app.use(logger('dev'))
app.use(bodyParser.json({limit: '5mb'}))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')))

 // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(auth.validateToken)
app.use(api)

module.exports = app
