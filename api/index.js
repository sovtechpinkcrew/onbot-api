'use strict'
let express = require('express')
let router = express.Router()
var firebase = require('firebase-admin')
var serviceAccount = require('../serviceAccountKey.json')
var fetch = require('node-fetch')
var uuidv4 = require('uuid/v4')

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: 'https://onbot-fb96e.firebaseio.com/'
})

router.get('/questions', (req, res) => {
  firebase.database().ref('/sovtech/questions').once('value').then((questionsSnapshot) => {
    let questions = questionsSnapshot.val()
    return res.json(questions)
  })
})

router.post('/questions', (req, res) => {
  let uuid = uuidv4()
  let data = req.body
  data.id = uuid
  firebase.database().ref('/sovtech/questions').push(data).then((questions) => {
    return res.json(questions)
  })
})

router.put('/questions/:ref', (req, res) => {
  firebase.database().ref('/sovtech/questions/' + req.params.ref).set(req.body).then((questions) => {
    return res.json(questions)
  })
})

router.delete('/questions/:ref', (req, res) => {
  firebase.database().ref('/sovtech/questions/' + req.params.ref).remove().then((questions) => {
    return res.json(questions)
  })
})

router.get('/answers', (req, res) => {
  firebase.database().ref('/sovtech/answers').once('value').then((answersSnapshot) => {
    let answers = answersSnapshot.val()
    return res.json(answers)
  })
})

router.post('/answers', (req, res) => {
  firebase.database().ref('/sovtech/answers').push(req.body).then((answers) => {
    return res.json(answers)
  })
})

router.put('/answers/:ref', (req, res) => {
  firebase.database().ref('/sovtech/answers/' + req.params.ref).set(req.body).then((answers) => {
    return res.json(answers)
  })
})

router.delete('/answers/:ref', (req, res) => {
  firebase.database().ref('/sovtech/answers/' + req.params.ref).remove().then((answers) => {
    return res.json(answers)
  })
})

router.get('/users', (req, res) => {
  firebase.database().ref('/sovtech/users').once('value').then((usersSnapshot) => {
    let users = usersSnapshot.val()
    return res.json(users)
  })
})

router.post('/users', (req, res) => {
  firebase.database().ref('/sovtech/users').push(req.body).then((users) => {
    return res.json(users)
  })
})

router.put('/users/:ref', (req, res) => {
  firebase.database().ref('/sovtech/users/' + req.params.ref).set(req.body).then((users) => {
    return res.json(users)
  })
})

router.delete('/users/:ref', (req, res) => {
  firebase.database().ref('/sovtech/users/' + req.params.ref).remove().then((users) => {
    return res.json(users)
  })
})

module.exports = router
