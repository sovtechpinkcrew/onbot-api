exports.validateToken = (req, res, next) => {
  if (req.headers['x-api-key'] === 'YWRtaW5Ab25ib3QubWU6Nzl3N240YmVHN0ptTU1oSg==') {
    next()
  } else {
    return res.status(401).json({ error: 'Invalid API key' })
  }
}
